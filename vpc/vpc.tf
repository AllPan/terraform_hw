variable "subnets" {
  type    = "list"
  default = ["us-east-1a", "us-east-1b", "us-east-1c", "us-east-1d", "us-east-1e", "us-east-1f"]
}

resource "aws_vpc" "Demo" {
  cidr_block           = "10.10.0.0/16"
  enable_dns_hostnames = true

  tags {
    Name = "Demo"
  }
}

resource "aws_internet_gateway" "GW" {
  vpc_id = "${aws_vpc.Demo.id}"
}

resource "aws_subnet" "pub" {
  count                   = 6
  cidr_block              = "10.10.${count.index+11}.0/24"
  vpc_id                  = "${aws_vpc.Demo.id}"
  availability_zone       = "${element(var.subnets, count.index)}"
  map_public_ip_on_launch = true

  tags {
    Name = "pub${count.index+1}"
  }
}

resource "aws_subnet" "priv" {
  count             = 6
  cidr_block        = "10.10.${count.index+21}.0/24"
  vpc_id            = "${aws_vpc.Demo.id}"
  availability_zone = "${element(var.subnets, count.index)}"

  tags {
    Name = "priv${count.index+1}"
  }
}

resource "aws_route_table" "pub" {
  vpc_id = "${aws_vpc.Demo.id}"

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.GW.id}"
  }

  tags {
    Name = "Public"
  }
}

resource "aws_route_table_association" "pub_sub" {
  count          = 6
  subnet_id      = "${element(aws_subnet.pub.*.id, count.index)}"
  route_table_id = "${aws_route_table.pub.id}"
}
