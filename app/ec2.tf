#variable "vpc" {
#  default = "Demo"
#}

data "aws_vpc" "sel" {
  tags {
    Name = "Demo"
  }
}

data "aws_subnet" "pub" {
  #  vpc_id = "${data.aws_vpc.sel.id}"
  tags {
    Name = "pub1"
  }
}

data "aws_subnet_ids" "pubs" {
  vpc_id = "${data.aws_vpc.sel.id}"

  tags {
    Name = "pub*"
  }
}

resource "aws_instance" "Pan_serv" {
  ami           = "ami-0ac019f4fcb7cb7e6"
  instance_type = "t2.micro"
  key_name      = "DevOps"

  #  availability_zone = "us-east-1a"
  vpc_security_group_ids = ["${aws_security_group.ec2sg.id}"]
  subnet_id              = "${data.aws_subnet.pub.id}"

  tags {
    Name = "demo-app-01"
  }
}

resource "aws_security_group" "ec2sg" {
  vpc_id = "${data.aws_vpc.sel.id}"
  name   = "es2sg1"

  ingress {
    cidr_blocks     = ["0.0.0.0/0"]
    from_port       = 22
    protocol        = "tcp"
    to_port         = 22
    security_groups = ["${aws_security_group.elbsg.id}"]
  }
  ingress {
    from_port       = 80
    protocol        = "tcp"
    to_port         = 80
    security_groups = ["${aws_security_group.elbsg.id}"]
  }

  egress {
    from_port   = 0
    protocol    = "-1"
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags {
    Name = "SG-EC2"
  }
}

resource "aws_security_group" "elbsg" {
  vpc_id = "${data.aws_vpc.sel.id}"
  name   = "es2sg"

  ingress {
    cidr_blocks = ["0.0.0.0/0"]
    from_port   = 80
    protocol    = "tcp"
    to_port     = 80
  }

  egress {
    from_port   = 0
    protocol    = "-1"
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags {
    Name = "SG-ELB"
  }
}

resource "aws_elb" "web" {
  subnets         = ["${data.aws_subnet_ids.pubs.ids}"]
  security_groups = ["${aws_security_group.elbsg.id}"]
  instances       = ["${aws_instance.Pan_serv.id}"]

  "listener" {
    instance_port     = 80
    instance_protocol = "http"
    lb_port           = 80
    lb_protocol       = "http"
  }

  tags {
    Name = "demo-app-01"
  }
}
