resource "aws_s3_bucket" "bucket" {
  bucket = "mysuperterraformbacket"

  versioning {
    enabled = false
  }

  tags {
    Name = "nginx-configuration"
  }
}
